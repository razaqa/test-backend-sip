#!/bin/bash
echo "SECRET_KEY=$SECRET_KEY_DJANGO" > .env.staging
python manage.py makemigrations --settings=sip.settings.staging
python manage.py migrate --settings=sip.settings.staging
python manage.py collectstatic --settings=sip.settings.staging